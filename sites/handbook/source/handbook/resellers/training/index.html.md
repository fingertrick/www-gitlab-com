---
layout: handbook-page-toc
title: 'Channel Partner Training, Certifications, and Enablement'
description: >-
  Learning program including functional, soft skills, and technical training for channel and alliances partners to support and scale GitLab's and our partners' growth and success
---

## Partner Training, Certifications, and Enablement
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

- - -

<span class="colour" style="color: rgb(0, 0, 0);">GitLab has created a robust training and certification program for partners in our community to sell, market, service, and support GitLab prospects and customers. Training materials and learning paths exist to support all roles within our partner’s organization including certifications for sales professionals, technical sales, and technical consulting services.</span>

## <span class="colour" style="color: rgb(252, 109, 38);">Why get certified?</span>

<span class="colour" style="color: rgb(0, 0, 0);">Certification allows an individual to validate their skills, knowledge, and mastery of GitLab. Certified partners can lead to increased engagement and facilitate knowledge sharing with customers. </span>

<span class="colour" style="color: rgb(0, 0, 0);">All GitLab training courses, certification exams, and supplemental enablement materials can be accessed through the </span>[GitLab Partner Portal](https://partners.gitlab.com/English/).

## <span class="colour" style="color: rgb(252, 109, 38);">Certifications for Sales Professionals</span>

<span class="colour" style="color: rgb(56, 13, 117);">![](https://lh4.googleusercontent.com/452hPpTIprFkJRlNm8IDPiMQPy1XJySXbFs6JFaYLcaTRtoYCqIr7x2C7Vt-FqteJxExxb8IVUN4DJl3INT0f2eSIeVh4F8IP8QgOCmcUaYQTLr25-ZjlTj80GTkThP2kCyivf6K)</span>

### <span class="colour" style="color: rgb(56, 13, 117);">GitLab Sales Core</span>

<span class="colour" style="color: rgb(0, 0, 0);">The GitLab Sales Core certification provides the fundamental knowledge for individuals new to selling and delivering GitLab services. The GitLab Sales Core certification is the prerequisite for all pre-sales technical and sales professionals. By completing this course and scoring above 80% on the exams, you will earn a GitLab Verified Sales Core completion badge and will contribute to the Sales Certification program requirements for your company. </span>

**Course Content Summary** (approx. 3 hours to complete)
- Introduction to GitLab culture and product
- Introduction to the DevOps Landscape
- Understanding GitLab Customers
- GitLab Portfolio Deep Dive
- Best Practices in Selling GitLab
- **Prerequisites**: There are no prerequisites for this course

### <span class="colour" style="color: rgb(56, 13, 117);">NEW! GitLab Sales Specialties</span>

<span class="colour" style="color: rgb(0, 0, 0);">GitLab Sales Specialties dive deeper into specified GitLab use cases including: DevOps Platform, Continuous Integration (CI), DevSecOps, and Agile Management.</span>

<span class="colour" style="color: rgb(56, 13, 117);">![](https://lh6.googleusercontent.com/MybqN9RgvYKGTQOBe2QAoXHPnGchEGPFAiL8rC-Bfu6_0QVDmI-F3PgsvhTxNWCoRq-_eu_0eAPPFKP7QACjFhJx3cKgOZYsYSZufTXBWWCDr-wh0mm0CJ9mJFTQzxOF0xi1IcL5)</span>

#### <span class="colour" style="color: rgb(56, 13, 117);">GitLab DevOps Platform Sales Specialist </span>

<span class="colour" style="color: rgb(0, 0, 0);">The GitLab Sales Specialist in DevOps Platform will prepare learners with an understanding of the DevOps lifecycle and how to increase their customers' operational efficiencies. Upon the completion of this course learners will be able to define a DevOps Platform and be able to position GitLab as the superior DevOps Platform. </span>

**Course Content Summary** (approx. 40 minutes to complete)
- What is a DevOps Platform?
- Target Personas
- Market Requirements
- The GitLab DevOps Platform
- **Prerequisites**: GitLab Sales Core Certification

![](https://lh3.googleusercontent.com/CeH-azgwuC6vCm0GXh7iXwUmUeREGR1BtrjggxCIoxkerHWhps0bGPMdHpZVCXkCsAwy8IMLV6NRvOOiFvdlW4900TRybQNaJZ6UaXQc_9Li4m9dodPBWYyikj4munKZNYjk186j)

#### <span class="colour" style="color: rgb(56, 13, 117);">GitLab DevSecOps Sales Specialist </span>

<span class="colour" style="color: rgb(0, 0, 0);">The DevSecOps Sales Specialist will prepare pre-sales professionals with industry relevant knowledge on the DevSecOps Market, Industry, and provide the best practices from GitLab in the DevSecOps space.</span>

**Course Content Summary (approx. 1 hour to complete)
- The DevSecOps Market
- Target Personas
- DevSecOps Industry Insights
- Market Requirements
- How GitLab Does DevSecOps
- **Prerequisites**: GitLab Sales Core Certification

![](https://lh3.googleusercontent.com/OrkQcg9vbspMjpn_doz9-sKOrLXZ4VtOkSYG7SjYF3iCcTbZZeUIvapfjs-3QTIToz0OKnZvo8bx0TPBylLdr_nSHIXRuYn1Ymn5khqhXDTRUZM3BvfBszKjP9qXw5tAAT4GY-7A)

#### <span class="colour" style="color: rgb(56, 13, 117);">GitLab Continuous Integration Sales Specialist </span>

<span class="colour" style="color: rgb(0, 0, 0);">The GitLab Continuous Integration Sales Specialist course provides learners with a fundamental framework of Continuous Integration (CI). At the end of this session, you will be able identify target personas for a CI conversation, understand the CI market, and position the GitLab CI solution.</span>

**Course Content Summary** (approx. 1 hour to complete)
- What is Continuous Integration?
- CI Market Requirements
- Target Personas
- GitLab’s CI Solution
- **Prerequisites**: GitLab Sales Core Certification

![](https://lh4.googleusercontent.com/ivNg05_7tM2AkOy9-nluuS0w5Y3XmmFjjCXireradrSFBvWu2Sx2nUdvTLWd-L6dA9zQ7U4rZaHv0Yh3L3IJoy8ZlACLL93dOCb77zC7eNilGSRlg2HWipy63i5p2JJjsJkNxHZP)

#### <span class="colour" style="color: rgb(56, 13, 117);">GitLab Agile Management Sales Specialist </span>

<span class="colour" style="color: rgb(0, 0, 0);">The GitLab Agile Management Sales Specialist course will guide learners through the basics of Agile Management, the Agile Management market, and the GitLab Agile Management Solution. At the end of this course, sales professionals will be able to position GitLab Agile Management as a part of the DevOpsPlatform solution. </span>

**Course Content Summary** (approx. 1 hour to complete)
- What is Agile Management?
- Agile Management Market Requirements
- Target Personas
- GitLab’s Agile Management Solution </span>

<span class="colour" style="color: rgb(0, 0, 0);"><u>Prerequisites</u></span>
<span class="colour" style="color: rgb(0, 0, 0);">GitLab Sales Core Certification </span>

**![](https://lh6.googleusercontent.com/QkygNV4N0W3BMWzANK8s1fpV2D6vI5CrKgvFPEh8wrdlC4ChEC4QJPz0OE4cgHXBkPF-XQyEwXkVCefbBXz5ccLJErGRCbJV-AKO-ckcWL-bdbJzqW8lP60Qj1m8H6y5RoAc5mRR)**

### <span class="colour" style="color: rgb(56, 13, 117);">**GitLab Sales Professional Certification**</span>

<span class="colour" style="color: rgb(0, 0, 0);">Upon the completion of the Verified Sales Core badge AND all four of the Sales Specialties, learners will be awarded the GitLab Sales Professional Certification. Certified GitLab Sales Professionals are able to identify GitLab target audiences, summarize the GitLab Solution Portfolio, and successfully position the value of GitLab to customers. </span>

**Certification Requirements (approx. 6 hours to complete)
- GitLab Sales Core Certification
- GitLab DevOps Sales Specialist
- GitLab DevSecOps Sales Specialist
- GitLab Continuous Integration Sales Specialist
- GitLab Agile Management Sales Specialist

## <span class="colour" style="color: rgb(252, 109, 38);">Certifications for Technical Sales Professionals</span>

**![](https://lh3.googleusercontent.com/qqbtMIKevnJhVdT6q1oqNWQ8B-rAU7EowMBYU-QtcTQS28pQhUrXn7T6dsbq24HyYIMS1bMuzIpQuNKLq730Tj9fWrsLSP-FQZpJGF2xGPX9fWTeWUo0iRtVDFsEhIq0kftc5rUK)**

### <span class="colour" style="color: rgb(56, 13, 117);">GitLab Solutions Architect Core</span>

<span class="colour" style="color: rgb(0, 0, 0);">This fundamental certification is designed for pre-sales technical professionals such as Solutions Architects or Sales Engineers and goes in depth demonstrating, deploying, integrating, and optimizing GitLab Solutions. The GitLab Solutions Architect Core meets the program requirements for pre-sales technical certification. </span>

**Course Content Summary** (approx 40 minutes to complete
- GitLab Integrations
- Technical Deep Dive
- **Prerequisites**: GitLab Sales Core Certification

### <span class="colour" style="color: rgb(56, 13, 117);">GitLab Professional Services Engineer (PSE)</span>

<span class="colour" style="color: rgb(0, 0, 0);">GitLab Professional Services Engineer (PSE) certification is a technical deep dive to help partners validate their ability to deliver implementation, migration, and consulting services to customers. This certification consists of six modules. Upon completion learners will possess the skills needed to deploy the GitLab Platform, migrate data to GitLab, and provide workflow advisory services for customers. This certification satisfies the PSE requirement for the GitLab [Certified Professional Services Partner Certification](https://about.gitlab.com/handbook/resellers/services/#gitlab-certified-professional-services-partner-requirements).

**Certification Requirements** (approx. 10 hours to complete)
- [Gitlab Implementation Specialist](https://about.gitlab.com/services/pse-certifications/implementation-specialist/)
- [GitLab Migration Specialist](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/gitlab-certified-migration-services-engineer/)
- [GitLab Certified Associate](https://about.gitlab.com/services/education/gitlab-certified-associate/)
- [GitLab CI/CD Specialist](https://about.gitlab.com/services/education/gitlab-cicd-specialist/)
- [GitLab Security Specialist](https://about.gitlab.com/services/education/gitlab-security-specialist/)
- [GitLab Project Management Specialist](https://about.gitlab.com/services/education/gitlab-project-management-specialist/)

## <span class="colour" style="color: rgb(56, 13, 117);">How do I enroll?</span>

<span class="colour" style="color: rgb(0, 0, 0);">To sign up for any of the courses listed on this page, please visit the training section on the [GitLab Partner Portal](https://partners.gitlab.com/prm/English/c/Training). If you are a new user, please allow one business day for the system to refresh and recognize your access in the EdCast GitLab Partner Channel. If you need additional support accessing training, please submit a ticket at support.gitlab.com and select “GitLab Learn” as the reason for your ticket.</span>

## <span class="colour" style="color: rgb(56, 13, 117);">Certification Award Process</span>

<span class="colour" style="color: rgb(0, 0, 0);">Upon successful completion of each learning path, individuals will be prompted to provide their email address in the course completion survey. Once the survey is submitted, individuals will receive their Badge of Completion to the email address provided. Please use the same email address as your Partner Portal log in. </span>

## <span class="colour" style="color: rgb(56, 13, 117);">Additional Enablement Resources</span>
- **Webcasts** - GitLab hosts a monthly webinar the first Thursday of each month, providing deep dive learning on key GitLab topics. You can access Partner Webcast Archives in the Asset Library on the [GitLab Partner Portal](https://partners.gitlab.com/English/).
- **Tech Chat** - Pre-Sales, Services Engineers and Solution Architects can join the Channel Solution Architects for a monthly Tech Chat. Use the **[Partner Tech Chat](https://gitlab.com/gitlab-com/partners/tech-chats)** board to submit future topics and questions.
- **Partner Marketing Webcasts** - GitLab Partners’ Marketing team members are invited to join our monthly webcast to learn about the latest Partner marketing campaigns, resources and more.
- **The [GitLab Partner Portal](https://partners.gitlab.com/English/)** - provides Partners with easy access to additional sales resources, webcast replays, competitive analysis, event calendar, Marketing campaigns, support and more.
- **Newsletter** - sign up [here](https://gitlab.us19.list-manage.com/subscribe?u=5a5f55e4e0f03037d96416766&id=2321e18463)
- **GitLab Handbook** - start your Handbook search on the [Channel Partner Handbook Page](https://about.gitlab.com/handbook/resellers/)
