---
layout: handbook-page-toc
title: "Interview Carousel - Becoming a better interviewer 15 minutes at a time"
description: "A lightweight and fun training to improve your user interviewing skills"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

At GitLab, everyone can contribute, which applies to UX Research, too. Product Managers and Product Designers regularly speak with users to validate the problem spaces and solutions they are working on and gain inspiration for what could come next.
Asking the right questions and listening actively is crucial in these conversations to get the information you need. 

The Interview Carousel is a lightweight, fun way to practice and enhance user interviewing skills for GitLab team members. Everyone can be a great interviewer, however, we are not necessarily born as one. It takes a little bit of practice, and that’s what the Interview Carousel is for. 

### The setup 

The Interview Carousel is a live (remote) event where team members get to speak with 3 to 4 users each. Imagine a speed-dating style setup where you speak with one user for 15 minutes and then switch to the next one. There are small breaks in between to avoid fatigue.

#### Discussion topics

As an Interview Carousel participant, you choose what you'd like to discuss with users. You can bring a prototype or talk about a specific item you are working (or planning to work) on. Alternately, you can simply learn more about that person, their job, and their life. Remember, it’s only 15 minutes per person, which goes by quickly. We aim to match you with users that represent the persona(s) of your stage.

#### How do I prepare?

There is a [20-minute video](https://youtu.be/b03eiIwz2LE) that you must watch before the event that includes tips and tricks on how to be a better interviewer. As you are watching the video, note the 2 or 3 things that you want to focus on and practice during the Interview Carousel.

Make sure to draft a [discussion guide](https://docs.google.com/document/d/1ERpTsQs7vcKKHLFZ5qoTukUFFA1sdazaFzknsX0Ju5Q/edit) for your session and share it for feedback with the UX Researcher who is hosting the Interview Carousel. This helps to ensure that participants won’t answer the same questions again and again. 

You should set aside about 1 hour to watch the training video and create your discussion guide.

#### What happens after the event?

To make the most out of the event, we strongly encourage you to record your interviews and upload them to [Dovetail](https://about.gitlab.com/handbook/engineering/ux/dovetail/). The UX Researcher hosting the Interview Carousel will watch the recordings and provide you with individual feedback on your interviewing skills.

### How do I join?

The next Interview Carousel is planned for **October 2022**. If you are interested in joining, fill out the [interest form](https://forms.gle/4s6SSobTcj1KJA5x8).

### Why should I join?

Below are some verbatims that GitLab team members who previously participated in an Interiew Carousel shared about their experience joining. 

_"This process made me more aware of ways that questions can be leading and ways to avoid it, especially in follow-up questions (it's hard!)."_

_"The video material was really good and helped to clarify my goals for every interview since then."_

_"This was the first opportunity I came across in my entire career as a designer to get my interviewing style assessed and get feedback."_

_"Valuable feedback! I usually don't notice I'm asking leading questions, so it's great to have it called out."_
