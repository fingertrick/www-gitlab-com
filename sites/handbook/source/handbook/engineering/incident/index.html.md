---
layout: markdown_page
title: "Incident"
---

## Definition

ToDo: What is an incident?

## Incident Management

ToDo: Describe overall process

## On-call Schedule Management

ToDo: High level description and link to relevant page

## How we monitor GitLab

ToDo: High level description and link to relevant page

## How do we respond to an ongoing incident

ToDo: High level description and link to relevant page

## Incident roles

ToDo: Should we consolidate incident roles here?