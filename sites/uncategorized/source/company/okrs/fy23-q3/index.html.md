---
layout: markdown_page
title: "FY23-Q3 OKRs"
description: "View GitLabs Objective-Key Results for FY23 Q3. Learn more here!"
canonical_path: "/company/okrs/fy23-q3/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from August 1, 2022 to October 31, 2022.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2022-06-27 | CEO shares top goals with E-group for feedback |
| -4 | 2022-07-04 | CEO pushes top goals to Ally.io |
| -4 | 2022-07-04 | E-group propose OKRs for their functions in Ally.io. These links are shared in #okrs Slack channel |
| -3 | 2022-07-11 | E-group 50 minute draft review meeting on 2022-04-11 |
| -2 | 2022-07-18 | E-group discusses with their respective teams and polish their OKRs |
| -1 | 2022-07-25 | CEO reports post links to final OKR in #okrs slack channel and @ mention the CEO and CoS to the CEO for approval |
| 0  | 2022-08-01 | CoS to the CEO updates OKR page for current quarter to be active and includes CEO level OKRs with consideration to what is public and non-public |


## OKRs

### 1. CEO 

### 2. CEO

### 3. CEO




